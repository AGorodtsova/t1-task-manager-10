package ru.t1.gorodtsova.tm.api;

import ru.t1.gorodtsova.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
