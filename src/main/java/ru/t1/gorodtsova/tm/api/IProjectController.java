package ru.t1.gorodtsova.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
