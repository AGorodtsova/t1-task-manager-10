package ru.t1.gorodtsova.tm.api;

import ru.t1.gorodtsova.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
