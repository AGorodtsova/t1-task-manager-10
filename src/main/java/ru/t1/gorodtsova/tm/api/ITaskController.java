package ru.t1.gorodtsova.tm.api;

public interface ITaskController {

    void createTask();

    void showTask();

    void clearTask();

}
